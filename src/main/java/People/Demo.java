package People;

import org.apache.log4j.*;

import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.stream.Stream;

public class Demo {

    private static Logger logger = LogManager.getLogger("main logger");

    public static void main(String[] args) throws IOException {
        final String format = "%d{yyyy-MM-dd HH:mm:ss} [%t] %p - %m%n";
        PatternLayout pattern = new PatternLayout(format);

        addConsoleAppender(logger, pattern);
        addFileAppender(logger, pattern);

        demoPeople();
    }

    private static void addFileAppender(Logger logger, PatternLayout pattern) throws IOException {
        final String logFile = "C:\\Users\\Paulius\\Documents\\log.txt";
        FileAppender fileAppender = new FileAppender(pattern, logFile);
        logger.addAppender(fileAppender);
        logger.info("hi");
        logger.error("fail");
        logger.warn("oops");
    }

    private static void demoPeople() {

        logger.info("Starting demo people");
        logger.info("18 yo and older: ");
        Person john = new Person("John", "Smith", 89, 190, Gender.MALE, LocalDate.of(1996, 1, 3));
        Person astrid = new Person("Astrid", "Oat", 57, 159, Gender.FEMALE, LocalDate.of(1999, 10, 10));
        Person steve = new Person("Steve", "Jobs", 68, 172, Gender.MALE, LocalDate.of(2002, 9, 15));

        Person[] persons = {john, astrid, steve};

        System.out.println("18 yo and older:");
        Stream<Person> peopleStream = Arrays.stream(persons);
        peopleStream
                .filter(p -> p.getAge() >= Person.ADULT_AGE)
                .forEach(p -> {
                    System.out.println(p);
                    logger.info(p);
                });

        logger.info("Ending demo people");
    }

    private static void addConsoleAppender(Logger logger, Layout layout) {
        ConsoleAppender consoleAppender = new ConsoleAppender(layout);
        logger.addAppender(consoleAppender);
    }
}
