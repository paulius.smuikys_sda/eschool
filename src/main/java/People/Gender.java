package People;

public enum Gender {
    FEMALE,
    MALE,
    OTHER
}
