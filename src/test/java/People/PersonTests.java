package People;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

public class PersonTests {

    @Test
    void getAge_when_person_bornToday_returns_0() {
        //Arange
        Person person = new Person("a", "b", 0, 0, Gender.MALE, LocalDate.now());

        //Act
        int age = person.getAge();

        //Aserts
        assertThat(age).isEqualTo(0);
    }

    @Test
    void getAge_when_person_bornInFuture() {
        //Arange
        Person person = new Person("a", "b", 0, 0, Gender.MALE, LocalDate.now());

        //Act
        LocalDate birthday = person.getBirthday();

        //Aserts
        assertThat(birthday).isBefore(LocalDate.now().plusDays(1));
    }

    @ParameterizedTest(name = "\"{0}\" - throws IllegalArgumentException")
    @ValueSource(strings = {"", " "})
    @NullSource
    void new_given_nullFirstName_throws_illegalArgumentException(String firstName) {
        // Act
        Throwable thrown = catchThrowable(() -> buildPersonWithFirstName(firstName));
        // Assert
        assertThat(thrown)
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessageContaining("firstName");
    }

    @ParameterizedTest(name = "\"{0}\" - throws IllegalArgumentException")
    @ValueSource(strings = {"", " "})
    @NullSource
    void new_given_nullLastName_throws_illegalArgumentException(String lastName) {
        // Act
        Throwable thrown = catchThrowable(() -> buildPersonWithLastName(lastName));
        // Assert
        assertThat(thrown)
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessageContaining("lastName");
    }

    private static Person buildPersonWithFirstName(String firstName) {
        return new Person(firstName, "b", 1, 1, Gender.MALE, LocalDate.now());
    }

    private static Person buildPersonWithLastName(String lastName) {
        return new Person("a", lastName, 1, 1, Gender.MALE, LocalDate.now());
    }
}
